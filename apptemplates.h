#ifndef APPTEMPLATE_H
#define APPTEMPLATE_H

#include <QWidget>


namespace Ui{
  class Widget;
}
class QModelIndex;
class QStringListModel;
struct zip;

class AppTemplates : public QWidget{
  Q_OBJECT
public:
  AppTemplates();

public slots:
  void indexActivated(const QModelIndex& index);
  void createProject();

private:
  Ui::Widget *ui;
  QStringListModel *model;
  struct zip *zipFile;

};

#endif
