
#include "apptemplates.h"
#include <QDir>
#include <QFile>
#include <QString>
#include <QDebug>
#include <QModelIndex>
#include <QStringListModel>
#include <zip.h>
#include "ui_widget.h"
#include <iostream>
#include <QDataStream>
#include <unistd.h>

AppTemplates::AppTemplates() : ui(new Ui::Widget()), zipFile(0){
      ui->setupUi(this);

      QDir::setCurrent( QCoreApplication::applicationDirPath()+"/templates");
      QDir dir;
      model = new QStringListModel(dir.entryList(QStringList("*.zip")));

      ui->listWidget->setModel(model);
	  ui->lineEdit->setText(QDir::homePath());

      connect(ui->listWidget, SIGNAL(activated(QModelIndex)), this, SLOT(indexActivated(QModelIndex)));
	  connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(createProject()));
}

void AppTemplates::indexActivated(const QModelIndex& index){
    QString completeFileName = QDir::currentPath() +QDir::separator()+ model->stringList().at(index.row());
    QByteArray array = completeFileName.toLocal8Bit();

	if (zipFile){
		zip_close(zipFile);
	}
    zipFile = zip_open(array.constData(),0, 0);

	zip_int64_t infoIndex = zip_name_locate(zipFile, "INFO", 0);

	struct zip_file *infoFile = zip_fopen(zipFile, "INFO", 0);
	struct zip_stat fileStatus;

	zip_stat(zipFile, "INFO", 0, &fileStatus);
	char *buffer = (char*) malloc(sizeof(char)* fileStatus.size);
	zip_fread(infoFile, buffer, fileStatus.size);
	ui->textBrowser->setText(QString(buffer));
	free(buffer);
}

void AppTemplates::createProject()
{
	if (!zipFile){
		qDebug() << "Arquivo nao aberto";
		return;
	}

	zip_uint64_t totalFiles = zip_get_num_entries(zipFile, 0) - 1;
	struct zip_stat fileStatus;

	for(zip_uint64_t i = 0; i < totalFiles; i++){
		zip_stat_index(zipFile, i, 0, &fileStatus);
		if (strcmp(fileStatus.name, "INFO") == 0){
			continue;
		}

		QString fileName(fileStatus.name);
		QFile f(ui->lineEdit->text()+QDir::separator()+fileName);

		if (fileName.endsWith(QDir::separator())){
			QFileInfo fInfo(f);
			QDir d;
			d.mkpath(fInfo.absolutePath());
		}


		f.open(QIODevice::WriteOnly);

		zip_file *archive = zip_fopen_index(zipFile, i, 0);
		char buf[100];
		int sum = 0;
		QDataStream stream(&f);
		while(sum != fileStatus.size){
			int read = zip_fread(archive, buf, 100);
			stream.writeRawData(buf, read);
			sum += read;
		}
		zip_fclose(archive);
	}

	zip_close(zipFile);
	zipFile = 0;
}
