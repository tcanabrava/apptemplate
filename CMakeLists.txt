project(apptemplates)
cmake_minimum_required(VERSION 2.6)
find_package(Qt4 REQUIRED)

set(CMAKE_AUTOMOC ON)

include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})

set(apptemplates_SRCS apptemplates.cpp main.cpp)

qt4_wrap_ui(WIDGET_UI_SRCS widget.ui)

add_executable(apptemplates ${apptemplates_SRCS} ${WIDGET_UI_SRCS})

target_link_libraries(apptemplates ${QT_QTCORE_LIBRARY} ${QT_QTGUI_LIBRARY} zip)

install(TARGETS apptemplates RUNTIME DESTINATION bin)
